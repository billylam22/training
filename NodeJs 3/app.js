const express = require('express');
var bodyParser = require('body-parser')
const { request } = require('http');
const app = express();

app.set('views', './views')
app.set('view engine', 'twig');
app.use('/public',express.static(__dirname+'/public'));
//The first public with forward slash stand for routing and the second public after forward slash stands for the folder name.

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json())


app.get('/home',function(request,response){
    response.render('index')
})

app.get('/', (req,res)=>{
    res.render('homepage.twig')
})

app.get('/contact', (req,res)=>{
    res.render('contact.twig')
})

app.post('/contact',(req,res)=>{
    console.log(req.body);
    res.render('userCredentials.twig',{
        userData : req.body
    })
})

app.get('/profile/:userId/:jobDescription',(req,res)=>{
    var userData = [
        {
            name : 'Jesse',
            age: '25',
            country: 'United Kingdom',
            hobbies: ['Basketball','Running','Coding','Watching Movies'],
            jobDescription: 'Web Developer',
            pets: ['Dog','Cat','Snake']
        },
    ]
    res.render('profile.twig',{
        username : req.params.userId,
        job : req.params.jobDescription,
        userData : userData
    })
})

app.get('/users',(req,res)=>{
    var userData = [
        {
            name : 'Jesse',
            age: '25',
            country: 'United Kingdom',
            hobbies: ['Basketball','Running','Coding','Watching Movies'],
            jobDescription: 'Web Developer',
            pets: ['Dog','Cat','Snake']
        },
        {
            name : 'Nicholina',
            age: '23',
            country: 'United Kingdom',
            hobbies: ['Singing','Music','Coding','Watching Movies'],
            jobDescription: 'Web Developer',
            pets: ['Dog','Cat']
        },
        {
            name : 'David',
            age: '24',
            country: 'United Kingdom',
            hobbies: ['Sleeping','Eating','Thinking','Breathing'],
            jobDescription: 'Web Developer',
            pets: ['Cactus']
        },
        {
            name : 'Michael',
            age: '20',
            country: 'United Kingdom',
            hobbies: ['Basketball','Running','Art'],
            jobDescription: 'Web Developer',
            pets: ['None']
        },
        {
            name : 'Kyla',
            age: '20',
            country: 'United Kingdom',
            hobbies: ['Music','Sewing','Reading'],
            jobDescription: 'Web Developer',
            pets: ['Dog','Plant']
        },
        {
            name : 'Marlon',
            age: '47',
            country: 'United Kingdom',
            hobbies: ['Football','Gym'],
            jobDescription: 'Web Developer',
            pets: ['Cat']
        },
    ]
    res.render('users.twig',{
        title: 'Users List',
        users: userData
    })
})

app.get('/condtionalStatements', (req,res)=>{
    var product = [
        {
            pName : 'HP OMEN Gaming Laptop',
            pPrice : '$1500',
            stock : 0
        }
    ]
res.render('condtionalStatements.twig',{
   online: false,
   websiteScore: 79,
   productDetails : product 
})
})


//For include method
app.get('/homepageWithInclude', (req,res)=>{
    res.render('forIncludeMethod/homepage.twig')
})
app.get('/contactWithInclude', (req,res)=>{
    res.render('forIncludeMethod/contact.twig')
})
//For include method


app.listen(3000,()=>{
    console.log('App is running on port 3000');
})